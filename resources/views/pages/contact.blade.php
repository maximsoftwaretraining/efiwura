@extends('layouts.app')

@section('title')
Contact Us
@endsection

@section('content')
<!-- Contact Start from here -->
<section class="at-contact-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="at-contact-form at-col-default-mar">
                    <div id="form-messages"></div>
                    <form id="ajax-contact" method="post" action="php/contact.php">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                        <input type="number" class="form-control" id="number" name="number" placeholder="Phone"
                            required>
                        <textarea class="form-control" id="message" name="message" rows="5" placeholder="Message"
                            required></textarea>
                        <button class="btn btn-default hvr-bounce-to-right" type="submit">Message</button>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="at-info-box at-col-default-mar">
                    <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>efiewura@gmail.com</span>
                </div>
                <div class="at-info-box at-col-default-mar">
                    <i class="fa fa-phone" aria-hidden="true"></i> <span> +233-567890</span>
                </div>
                <div class="at-info-box at-col-default-mar">
                    <i class="fa fa-map-marker" aria-hidden="true"></i> <span>Tranta Hills-Coral Avenue,Accra</span>
                </div>
            </div>
        </div>
    </div>
</section>
<!--  Contact end-->

<div id="googleMap" style="width:100%; height:400px;"></div>


@endsection