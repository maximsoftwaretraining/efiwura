@extends('layouts.app')

@section('title')
Advertise
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">
@endsection
@section('content')


<section class="at-agents-sec"
    style="background-image: url('https://images.pexels.com/photos/1571463/pexels-photo-1571463.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940')">
    <div class="container" style=" margin-top:-80px;">

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="shadow p-4 bg-white" style="border-radius: 20px;">
                    {{-- Property tab start --}}
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="text-warning nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Ad Info</a>
                            <a class="text-warning nav-item nav-link" id="nav-profile-tab" data-toggle="tab"
                                href="#nav-profile" role="tab" aria-controls="nav-profile"
                                aria-selected="false">Location</a>
                            <a class="text-warning nav-item nav-link" id="nav-contact-tab" data-toggle="tab"
                                href="#nav-contact" role="tab" aria-controls="nav-contact"
                                aria-selected="false">Features</a>
                            <a class="text-warning nav-item nav-link" id="nav-image-tab" data-toggle="tab"
                                href="#nav-image" role=" tab" aria-controls="nav-image" aria-selected="false">Images</a>
                        </div>
                    </nav>
                    {{-- Property tab end --}}

                    {{-- Property form start --}}
                    <form action="{{ route('property.store') }}" enctype="multipart/form-data" method="POST">

                        <div class="tab-content" id="nav-tabContent">

                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                aria-labelledby="nav-home-tab">
                                <div class="form-group">
                                    <label for="firstname">Title</label>
                                    <input type="text" id="title" class="form-control" name="firstname" />
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea name="description" id="description" class="form-control"
                                        placeholder="Describe your property" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="property_type">Property Type</label>
                                    <select class="form-control" name="property_type" id="property_type">
                                        <option value="">Select One</option>
                                        <option value="">Building</option>
                                        <option value="">Land</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="property_type">Category</label>
                                    <select class="form-control" name="property_type" id="property_type">
                                        <option value="">Select One</option>
                                        <option value="">For Sale</option>
                                        <option value="">For Rent</option>
                                    </select>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                aria-labelledby="nav-profile-tab">

                                <div class="form-group">
                                    <label for="">Price</label>
                                    <input type="text" id="lastname" class="form-control" name="lastname" />
                                </div>
                                <div class="form-group">
                                    <label for="property_type">Status</label>
                                    <select class="form-control" name="property_type" id="property_type">
                                        <option value="">Select One</option>
                                        <option value="">Old</option>
                                        <option value="">New</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="property_type">Region</label>
                                    <select class="form-control" name="property_type" id="property_type">
                                        <option value="">Select One</option>
                                        <option value="">Accra</option>
                                        <option value="">Sunyani</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Address</label>
                                    <input type="text" id="address" class="form-control" name="lastname" />
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                                aria-labelledby="nav-contact-tab">
                                <div class="form-group">
                                    <label for="property_type">Features</label>
                                    <select class="form-control" name="property_type" id="property_type">
                                        <option value="">Select One</option>
                                        <option value="">Old</option>
                                        <option value="">New</option>
                                    </select>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-image" role="tabpanel" aria-labelledby="nav-image-tab">

                                <div class="form-group">
                                    <label for="">Upload Some images</label>
                                    <div class="dropzone" id="myID"></div>
                                </div>
                            </div>
                        </div>



                        <button type="submit" class="btn btn-success" id="submit-all"> Submit
                            Property </button>
                    </form>
                    {{-- Property form end --}}
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
<script src="{{ asset('js/dropzone.min.js') }}"></script>
<script>
    Dropzone.autoDiscover = false;
    $(document).ready(function(){
    var myDropzone = new Dropzone("div#myID",{
    url:'/property',
    autoProcessQueue: false,
    uploadMultiple: true,
    parallelUploads: 15,
    maxFiles: 15,
    maxFilesize: 5,
    acceptedFiles: 'image/*',
    addRemoveLinks: true,
    init: function() {
        dzClosure = this; 

        document.getElementById("submit-all").addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            dzClosure.processQueue();
        });

        this.on("sendingmultiple", function(data, xhr, formData) {
            formData.append("title", jQuery("#title").val());
            formData.append("description", jQuery("#description").val());
            formData.append("_token",'{{ csrf_token() }}')
        });
        this.on('success',function(response){
            alert('success')
        })
      
    }
});
  })
</script>
<script>
    function activateSearch(){
        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
</script>
<script type="text/javascript"
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCO-OqN7BviYppgz6rrxZLIa7gOElhuYb8&libraries=places&callback=activateSearch">
</script>
@endsection