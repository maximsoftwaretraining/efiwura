@extends('layouts.app')

@section('title')
Welcome to Property Page
@endsection

@section('content')
<!-- Main Search start from here -->
<section class="main-search-field">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="at-col-default-mar">
                    <select>
                        <option value="0" selected>Location</option>
                        <option value="1">Alabama</option>
                        <option value="2">Alaska</option>
                        <option value="3">California</option>
                        <option value="4">Colorado</option>
                        <option value="5">Delaware</option>
                        <option value="6">District of Columbia</option>
                    </select>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="at-col-default-mar">
                    <select class="div-toggle" data-target=".my-info-1">
                        <option value="0" data-show=".acitveon" selected>Property Status</option>
                        <option value="1" data-show=".sale">For Sale</option>
                        <option value="2" data-show=".rent">For Rent</option>
                    </select>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="at-col-default-mar">
                    <input class="at-input" type="text" name="min-area" placeholder="Squre Fit Min">
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="at-col-default-mar">
                    <input class="at-input" type="text" name="max-area" placeholder="Squre Fit Max">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="at-col-default-mar">
                    <select>
                        <option value="0" selected>Bedroom</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="at-col-default-mar">
                    <select>
                        <option value="0" selected>Bathroom</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="at-col-default-mar">
                    <div class="at-pricing-range">
                        <div class="my-info-1">
                            <h4>At first select Property Status</h4>
                            <div class="acitveon sale hide">
                                <label>Price : </label>
                                <input type="text" class="amount at-input-price" readonly>
                                <div class="slider-range"></div>
                            </div>
                            <div class="rent hide">
                                <label>Price : </label>
                                <input type="text" class="amount-two at-input-price" readonly>
                                <div class="slider-range-two"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="at-col-default-mar">
                    <button class="btn btn-default hvr-bounce-to-right" type="submit">Filter</button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Main Search End -->

<!-- Property start from here -->
<section class="at-property-sec at-property-right-sidebar">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="at-sidebar">
                    <div class="at-sidebar-search at-sidebar-mar">
                        <form method="post">
                            <div class="input-group">
                                <input placeholder="Search Here....." class="form-control" name="search-field"
                                    type="text">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="at-categories clearfix">
                        <h3 class="at-sedebar-title">categories</h3>
                        <ul>
                            <li><a href="#">Greater Accra</a> <span class="pull-right">(10)</span>
                            </li>
                            <li><a href="#">Westhern Region</a> <span class="pull-right">(08)</span>
                            </li>
                            <li><a href="#">Easthern Region</a> <span class="pull-right">(29)</span>
                            </li>
                            <li><a href="#">Volta Region</a> <span class="pull-right">(33)</span>
                            </li>
                            <li><a href="#">Northern Region</a> <span class="pull-right">(23)</span>
                            </li>
                            <li><a href="#">Upper East Region</a> <span class="pull-right">(22)</span>
                            </li>
                            <li><a href="#">Upper West Region</a> <span class="pull-right">(29)</span>
                            </li>
                            <li><a href="#">Ashanti Region</a> <span class="pull-right">(11)</span>
                            </li>
                            <li><a href="#">Bono East Region</a> <span class="pull-right">(10)</span>
                            </li>
                            <li><a href="#">Ahafo Region</a> <span class="pull-right">(08)</span>
                            </li>
                            <li><a href="#">Brong Ahafo Region</a> <span class="pull-right">(29)</span>
                            </li>
                            <li><a href="#">North East Region</a> <span class="pull-right">(33)</span>
                            </li>
                            </li>
                            <li><a href="#">Savanna Region</a> <span class="pull-right">(33)</span>
                            </li>
                            <li><a href="#">Oti Region</a> <span class="pull-right">(29)</span>
                            </li>
                            <li><a href="#">Westhern North Region</a> <span class="pull-right">(33)</span>
                            </li>
                            </li>
                            <li><a href="#">Central Region</a> <span class="pull-right">(33)</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="at-property-item at-col-default-mar shadow p-2">
                            <div class="at-property-img">
                                <img src="images/property/1.jpg" alt="">
                                <div class="at-property-overlayer"></div>
                                <a class="btn btn-default at-property-btn"
                                    href="{{ route('details',['slug'=>'xfshshgs']) }}" role="button">View Details</a>
                                <h4>For Sale</h4>
                                <h5>Ȼ59,999</h5>
                            </div>
                            <div class="at-property-dis">
                                <ul>
                                    <li><i class="fa fa-object-group" aria-hidden="true"></i> 520 sq ft</li>
                                    <li><i class="fa fa-bed" aria-hidden="true"></i> 6</li>
                                    <li><i class="fa fa-bath" aria-hidden="true"></i> 3</li>
                                </ul>
                            </div>
                            <div class="at-property-location">
                                <h4><i class="fa fa-home" aria-hidden="true"></i><a
                                        href="{{ route('details',['slug'=>'xfshshgs']) }}">New Superb Villa</a></h4>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> Sakasaka main Road, Tamale, NR-GH
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="at-property-item at-col-default-mar shadow p-2">
                            <div class="at-property-img">
                                <img src="images/property/2.jpg" alt="">
                                <div class="at-property-overlayer"></div>
                                <a class="btn btn-default at-property-btn"
                                    href="{{ route('details',['slug'=>'xfshshgs']) }}" role="button">View Details</a>
                                <h4 class="at-bg-black">For Rent</h4>
                                <h5 class="at-bg-black">Ȼ59,999</h5>
                            </div>
                            <div class="at-property-dis">
                                <ul>
                                    <li><i class="fa fa-object-group" aria-hidden="true"></i> 520 sq ft</li>
                                    <li><i class="fa fa-bed" aria-hidden="true"></i> 6</li>
                                    <li><i class="fa fa-bath" aria-hidden="true"></i> 3</li>
                                </ul>
                            </div>
                            <div class="at-property-location">
                                <h4><i class="fa fa-home" aria-hidden="true"></i><a
                                        href="{{ route('details',['slug'=>'xfshshgs']) }}">New Superb Villa</a></h4>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> Opp. GCB main Bank, Ho, VR-GH</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="at-property-item at-col-default-mar shadow p-2">
                            <div class="at-property-img">
                                <img src="images/property/3.jpg" alt="">
                                <div class="at-property-overlayer"></div>
                                <a class="btn btn-default at-property-btn"
                                    href="{{ route('details',['slug'=>'xfshshgs']) }}" role="button">View Details</a>
                                <h4>For Sale</h4>
                                <h5>Ȼ59,999</h5>
                            </div>
                            <div class="at-property-dis">
                                <ul>
                                    <li><i class="fa fa-object-group" aria-hidden="true"></i> 520 sq ft</li>
                                    <li><i class="fa fa-bed" aria-hidden="true"></i> 6</li>
                                    <li><i class="fa fa-bath" aria-hidden="true"></i> 3</li>
                                </ul>
                            </div>
                            <div class="at-property-location">
                                <h4><i class="fa fa-home" aria-hidden="true"></i><a
                                        href="{{ route('details',['slug'=>'xfshshgs']) }}">New Superb Villa</a></h4>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> Near Overhead, Aboabo, Kumasi,
                                    AR-GH</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="at-property-item at-col-default-mar shadow p-2">
                            <div class="at-property-img">
                                <img src="images/property/4.jpg" alt="">
                                <div class="at-property-overlayer"></div>
                                <a class="btn btn-default at-property-btn"
                                    href="{{ route('details',['slug'=>'xfshshgs']) }}" role="button">View Details</a>
                                <h4 class="at-bg-black">For Rent</h4>
                                <h5 class="at-bg-black">Ȼ59,999</h5>
                            </div>
                            <div class="at-property-dis">
                                <ul>
                                    <li><i class="fa fa-object-group" aria-hidden="true"></i> 520 sq ft</li>
                                    <li><i class="fa fa-bed" aria-hidden="true"></i> 6</li>
                                    <li><i class="fa fa-bath" aria-hidden="true"></i> 3</li>
                                </ul>
                            </div>
                            <div class="at-property-location">
                                <h4><i class="fa fa-home" aria-hidden="true"></i><a
                                        href="{{ route('details',['slug'=>'xfshshgs']) }}">New Superb Villa</a></h4>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> Adj. Main VRA Office, Accra,
                                    GA-GH</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="at-pagination">
            <ul class="pagination justify-content-end">
                <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-double-left"></i></a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">4</a></li>
                <li class="page-item"><a class="page-link" href="#">5</a></li>
                <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-double-right"></i></a></li>
            </ul>
        </div>
    </div>
</section>
<!-- Property End -->
@endsection