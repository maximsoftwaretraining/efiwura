@extends('layouts.app')

@section('title')
Property Details
@endsection
@section('css')
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
<link href="{{ asset('css/star-rating.css') }}" media="all" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<!-- Property start from here -->
<section class="at-property-sec at-property-right-sidebar" style="margin-top: -50px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="at-sec-title">
                    <h2>3 Bedroom house for sale at Kasoa</h2>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="at-property-details-col">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="{{ asset('images/property/p1.jpg') }}" alt="">

                            </div>
                            <!-- End Item -->
                            <div class="carousel-item">
                                <img src="{{ asset('images/property/p2.jpg') }}" alt="">

                            </div>
                            <!-- End Item -->
                            <div class="carousel-item">
                                <img src="{{ asset('images/property/p3.jpg') }}" alt="">

                            </div>
                            <!-- End Item -->
                            <div class="carousel-item">
                                <img src="{{ asset('images/property/p4.jpg') }}" alt="">

                            </div>
                            <!-- End Item -->
                        </div>
                        <!-- End Carousel Inner -->
                        <ul class="nav nav-pills nav-justified">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active">
                                <a href="#"><img src="{{ asset('images/property/p1.jpg') }}" alt="">
                                </a>
                            </li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1">
                                <a href="#"><img src="{{ asset('images/property/p2.jpg') }}" alt="">
                                </a>
                            </li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2">
                                <a href="#"><img src="{{ asset('images/property/p3.jpg') }}" alt="">
                                </a>
                            </li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="3">
                                <a href="#"><img src="{{ asset('images/property/p4.jpg') }}" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- End Carousel -->
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page
                        when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal
                        distribution of letters, as opposed to using 'Content here, content here', making it look like
                        readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as
                        their default model text, and a search for 'lorem ipsum' will uncover many web sites still in
                        their infancy. It is a long established fact that a reader will be distracted by the readable
                        content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a
                        more-or-less normal distribution of letters, as opposed to using 'Content here, content here',
                        making it look like readable English. Many desktop publishing packages and web page editors now
                        use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many
                        web sites still in their infancy. </p>
                    <div class="at-sec-title at-sec-title-left">
                        <h2>Property <span>Features</span></h2>
                        <div class="at-heading-under-line">
                            <div class="at-heading-inside-line"></div>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem
                            porro tempore temporibus ducimus</p>
                    </div>
                    <div class="row at-property-features">
                        <div class="col-md-6 clearfix">
                            <ul>
                                <li>Property ID : <span class="pull-right">AB-010234</span>
                                </li>
                                <li>Full Area : <span class="pull-right">520 sqft</span>
                                </li>
                                <li>Bedrooms : <span class="pull-right">6</span>
                                </li>
                                <li>Bathrooms : <span class="pull-right">3</span>
                                </li>
                                <li>Garages : <span class="pull-right">1</span>
                                </li>
                                <li>swimming pool : <span class="pull-right">Yes</span>
                                </li>
                                <li>Party Rooms : <span class="pull-right">Yes</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul>
                                <li>Status : <span class="pull-right"> for Sale</span>
                                </li>
                                <li>Kitchen : <span class="pull-right">2</span>
                                </li>
                                <li>AC Rooms: <span class="pull-right">4</span>
                                </li>
                                <li>Internet : <span class="pull-right">Yes</span>
                                </li>
                                <li>Cable TV : <span class="pull-right">Yes</span>
                                </li>
                                <li>Balcony : <span class="pull-right">Yes</span>
                                </li>
                                <li>Pool : <span class="pull-right">Yes</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="at-comment-row">
                                <h3><a href="#">Comment(3)</a></h3>
                                <div class="at-comment-item">

                                    <img src="https://www.venmond.com/demo/vendroid/img/avatar/big.jpg" alt="">
                                    <h5>Alin Max</h5>
                                    <span>3 hours ago</span>
                                    <p>It is a long established fact that a reader will be distracted by the readable
                                        content of a page when looking at its layout. The point of using Lorem Ipsum is
                                        that. </p>
                                </div>
                                <div class="at-comment-item">

                                    <img src="https://www.venmond.com/demo/vendroid/img/avatar/big.jpg" alt="">
                                    <h5>Max Julio</h5>
                                    <span>2 hours ago</span>
                                    <p>It is a long established fact that a reader will be distracted by the readable
                                        content of a page when looking at its layout. The point of using Lorem Ipsum is
                                        that. </p>
                                </div>
                                <div class="at-comment-item">

                                    <img src="https://www.venmond.com/demo/vendroid/img/avatar/big.jpg" alt="">
                                    <h5>Ross Tailor</h5>
                                    <span>5 hours ago</span>
                                    <p>It is a long established fact that a reader will be distracted by the readable
                                        content of a page when looking at its layout. The point of using Lorem Ipsum is
                                        that. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="at-form-area">
                        <div class="row">
                            <div class="col-lg-12">
                                <form id="contact_form" action="" method="post">
                                    <div class="row">

                                        <div class="col-md-12 col-sm-12">
                                            <textarea class="form-control" name="message" rows="5"
                                                placeholder="Write message here"></textarea>
                                            <button class="btn btn-default hvr-bounce-to-right"
                                                type="submit">SUBMIT</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="at-sidebar at-col-default-mar">
                    <div class="at-sidebar-search at-sidebar-mar">
                        <div class="card" style="padding:40px;">
                            <img class="rounded-circle" width="100px" height="100px"
                                src="https://thumbor.forbes.com/thumbor/960x0/https%3A%2F%2Fblogs-images.forbes.com%2Fkarineldor%2Ffiles%2F2018%2F05%2FJennifer-Mullowney-1200x1200.jpg"
                                class="card-img-top" alt="">
                            <div class="card-body">
                                <h5 class="card-title"> <b>Owner :</b> Rita Otwells Queen</h5>
                                <h5 class="card-title"> <b>Contact :</b> +233267499991</h5>
                                <p class="card-text">
                                    <input id="rating" name="rating" data-display-only="true" data-show-caption="false"
                                        value="2.5" class="rating">
                                </p>
                                <a href="#" class="btn btn-warning">View Profile</a>
                            </div>
                        </div>
                    </div>

                    <div class="at-latest-news">
                        <h3 class="at-sedebar-title">Other Properties</h3>
                        <ul>
                            <li>
                                <div class="at-news-item">
                                    <img src="https://images.pexels.com/photos/584399/living-room-couch-interior-room-584399.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                        alt="">
                                    <h4><a href="#">Popular building design</a></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis unde</p>
                                </div>
                            </li>
                            <li>
                                <div class="at-news-item">
                                    <img src="https://images.pexels.com/photos/584399/living-room-couch-interior-room-584399.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                        alt="">
                                    <h4><a href="#">Best building design</a></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis unde</p>
                                </div>
                            </li>
                            <li>
                                <div class="at-news-item">
                                    <img src="https://images.pexels.com/photos/584399/living-room-couch-interior-room-584399.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                        alt="">
                                    <h4><a href="#">Building in city</a></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis unde</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('js')
<script src="{{ asset('js/star-rating.js') }}" type="text/javascript"></script>
{{-- <script>
    $(document).ready(function(){
        $('#rating').rating({
            step: 1,
            starCaptions: {1: 'Very Poor', 2: 'Poor', 3: 'Ok', 4: 'Good', 5: 'Very Good'},
            starCaptionClasses: {1: 'text-danger', 2: 'text-warning', 3: 'text-info', 4: 'text-primary', 5: 'text-success'},
            displayOnly:true,
           
        });
    });
    </script> --}}
@endsection