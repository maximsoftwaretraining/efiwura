@extends('layouts.app')

@section('title')
About
@endsection

@section('content')
{{-- About start --}}
<section class="at-agents-sec">
    <div class="container" style=" margin-top:-80px;">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="at-sec-title">
                    <h3>About Efiwura</span></h3>

                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row animatedParent animateOnce">
            {{-- About efiwura start --}}
            <div class="col-xl-7 col-lg-6 col-md-12">
                <div class="at-about-col at-col-default-mar">

                    <div class="at-about-title">
                        <h4>Few description about Efiwura</h4>
                        <h6>Real Estate</h6>
                    </div>

                    <p>
                        There are many variations of passages of Lorem Ipsum available,
                        but the majority have suffered alteration in some form, by injected humour,
                        or randomised words which dont look even slightly believable.
                        If you are going to a passage of Lorem Ipsum.
                    </p>

                    <p>
                        There are many variations of passages of Lorem Ipsum available,
                        but the majority have suffered alteration in some form, by injected humour,
                        or randomised words which dont look even slightly believable.
                        If you are going to a passage of Lorem Ipsum.
                    </p>

                </div>
            </div>
            {{-- About efiwura end --}}
            {{-- About Image start --}}
            <div class="col-xl-5 col-lg-6 col-md-6">
                <div class="at-about-col animated fadeInRightShort slow delay-250">
                    <img class="img img-thumbnail" src="images/about/2.jpg" alt="image" style="height:300px">
                </div>
            </div>
            {{-- About image end --}}
        </div>
        <hr>
        {{-- Mission vission starts here --}}
        <div class="row">

            <div class="col-md-4 mb-2">
                <div class="card card-default">
                    <div class="at-sec-title at-sec-title-left">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fa fa-balance-scale"></i>
                                OUR MISSION
                            </h3>
                        </div>
                        <div class="card-body">
                            <p> There are many variations of passages of Lorem Ipsum available,
                                but the majority have suffered alteration in some form, by injected humour,
                                or randomised words which dont look even slightly believable.
                                If you are going to a passage of Lorem Ipsum.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mb-2">
                <div class="card card-default">
                    <div class="at-sec-title at-sec-title-left">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fa fa-eye fa-fw"></i>
                                OUR VISION
                            </h3>
                        </div>
                        <div class="card-body">
                            <p>
                                There are many variations of passages of Lorem Ipsum available,
                                but the majority have suffered alteration in some form, by injected humour,
                                or randomised words which dont look even slightly believable.
                                If you are going to a passage of Lorem Ipsum.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mb-2">
                <div class="card card-default">
                    <div class="at-sec-title at-sec-title-left">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fa fa-diamond"></i>
                                OUR VALUES
                            </h3>
                        </div>
                        <div class="card-body">
                            <p>There are many variations of passages of Lorem Ipsum available,
                                but the majority have suffered alteration in some form, by injected humour,
                                or randomised words which dont look even slightly believable.
                                If you are going to a passage of Lorem Ipsum.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        {{-- Mission vission ends here --}}
    </div>

</section>

<!-- About End -->

@endsection