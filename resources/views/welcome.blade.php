@extends('layouts.app')

@section('title')
Welcome
@endsection

@section('content')
<!-- Main Search start from here -->
<section class="main-search-field main-search-field-two main-search-field-three at-over-layer-black"
    id="water-animation">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="at-col-default-mar clearfix">
                    <h1 id="text-animation">We are real estate company, Find your dream home from here, We are very
                        helpful, We are very modern, We are very experienced</h1>
                </div>
            </div>
        </div>
        <div class="at-search-box">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="at-col-default-mar">
                        <select>
                            <option value="0" selected>Location</option>
                            <option value="1">Alabama</option>
                            <option value="2">Alaska</option>
                            <option value="3">California</option>
                            <option value="4">Colorado</option>
                            <option value="5">Delaware</option>
                            <option value="6">District of Columbia</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="at-col-default-mar">
                        <select class="div-toggle" data-target=".my-info-1">
                            <option value="0" data-show=".acitveon" selected>Property Status</option>
                            <option value="1" data-show=".sale">For Sale</option>
                            <option value="2" data-show=".rent">For Rent</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="at-col-default-mar">
                        <input class="at-input" type="text" name="min-area" placeholder="Squre Fit Min">
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="at-col-default-mar">
                        <input class="at-input" type="text" name="max-area" placeholder="Squre Fit Max">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="at-col-default-mar">
                        <select>
                            <option value="0" selected>Bedroom</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="at-col-default-mar">
                        <select>
                            <option value="0" selected>Bathroom</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="at-col-default-mar">
                        <div class="at-pricing-range">
                            <div class="my-info-1">
                                <h4>At first select Property Status</h4>
                                <div class="acitveon sale hide">
                                    <label>Price : </label>
                                    <input type="text" class="amount at-input-price" readonly>
                                    <div class="slider-range"></div>
                                </div>
                                <div class="rent hide">
                                    <label>Price : </label>
                                    <input type="text" class="amount-two at-input-price" readonly>
                                    <div class="slider-range-two"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="at-col-default-mar">
                        <button class="btn btn-default hvr-bounce-to-right" type="submit">Search</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Main Search End -->

<!-- About start from here -->
<section class="container">
    <div class="row justify-content-center">
        <div class="col-lg-6 mt-5">
            <div class="at-sec-title">
                <h2>What <span>Separates Us</span></h2>
                <div class="at-heading-under-line">
                    <div class="at-heading-inside-line"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-6 mb-4">
            <p class="text-center"><img style="height: 60px !important; width:60px !important;"
                    src="https://previews.123rf.com/images/schlaumal/schlaumal1804/schlaumal180400124/99408715-mobile-phone-rating-with-5-stars-flat-design-icon.jpg"
                    alt=""></p>
            <h2 class="text-center">Rating Property Owners</h2>
            <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia voluptatibus ipsam
                adipisci provident perferendis, maiores tempore! Esse, aspernatur molestias soluta a consequuntur
                officiis nam, at quaerat possimus dolor, blanditiis minus!</p>
        </div>
        <div class="col-md-6 mb-4">
            <p class="text-center"><img style="height: 60px !important; width:60px !important;"
                    src="https://www.pngitem.com/pimgs/m/34-340984_flat-icon-tutorial-home-icon-flat-design-hd.png"
                    alt=""></p>
            <h2 class="text-center">Advertising</h2>
            <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia voluptatibus ipsam
                adipisci provident perferendis, maiores tempore! Esse, aspernatur molestias soluta a consequuntur
                officiis nam, at quaerat possimus dolor, blanditiis minus!</p>
        </div>
        <div class="col-md-6 mb-4">
            <p class="text-center"><img style="height: 60px !important; width:60px !important;"
                    src="https://www.kindpng.com/picc/m/111-1110978_house-icon-flat-home-png-icon-transparent-png.png"
                    alt=""></p>
            <h2 class="text-center">Easy Search For Property</h2>
            <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia voluptatibus ipsam
                adipisci provident perferendis, maiores tempore! Esse, aspernatur molestias soluta a consequuntur
                officiis nam, at quaerat possimus dolor, blanditiis minus!</p>
        </div>
        <div class="col-md-6 mb-4">
            <p class="text-center"><img style="height: 60px !important; width:60px !important;"
                    src="https://icons.iconarchive.com/icons/graphicloads/100-flat/256/home-icon.png" alt=""></p>
            <h2 class="text-center">Accessibility</h2>
            <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia voluptatibus ipsam
                adipisci provident perferendis, maiores tempore! Esse, aspernatur molestias soluta a consequuntur
                officiis nam, at quaerat possimus dolor, blanditiis minus!</p>
        </div>
    </div>
</section>
<!-- About End -->

<!-- Property start from here -->
<section class="at-property-sec">
    <div class="container" style="margin-top:-60px;">

        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="at-sec-title">
                    <h2>Available <span>Properties</span></h2>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row animatedParent animateOnce">
            @foreach ([1,2,3,4,5,6,6,7,8] as $item)

            <div class="col-md-4 col-sm-6  ">
                <div class="at-property-item at-col-default-mar animated fadeInUpShort slow shadow p-2">
                    <div class="at-property-img">
                        <img src="https://devtraco.com/wp-content/uploads/2019/10/4-bedroom-house-sale-in-ghana-thumbs.jpg"
                            alt="">
                        <div class="at-property-overlayer"></div>
                        <a class="btn btn-default at-property-btn" href="{{ route('details',['slug'=>'xfshshgs']) }}"
                            role="button">View Details</a>
                        <h4>For Sale</h4>
                        <h5>$59,999</h5>
                    </div>
                    <div class="at-property-dis">
                        <ul>
                            <li><i class="fa fa-object-group" aria-hidden="true"></i> 520 sq ft</li>
                            <li><i class="fa fa-bed" aria-hidden="true"></i> 6</li>
                            <li><i class="fa fa-bath" aria-hidden="true"></i> 3</li>
                        </ul>
                    </div>
                    <div class="at-property-location">
                        <h4><i class="fa fa-home" aria-hidden="true"></i><a
                                href="{{ route('details',['slug'=>'xfshshgs']) }}">New Superb Villa</a></h4>
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> 123 1st Width Road, , summit, new york
                        </p>
                    </div>
                </div>
            </div>

            @endforeach
            <div class="col-md-12 col-sm-12 text-center">
                <a class="btn btn-default hvr-bounce-to-right" href="properties-col-3.html" role="button"
                    style="border-radius:3em">More Properties</a>
            </div>
        </div>
    </div>
</section>
<!-- Property End -->

<!-- Agents start from here -->
<section class="at-agents-sec" style="margin-top:-40px;">
    <div class="container" style=" margin-top:-80px;">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="at-sec-title">
                    <h2>Top Rated <span>Property Owners</span></h2>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="agent-carousel" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>
                    <div class="at-agent-col">
                        <div class="at-agent-img">
                            <img src="https://images.pexels.com/photos/756453/pexels-photo-756453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt="">
                            <div class="at-agent-social">
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <div class="at-agent-call">
                                    <p>Stars</p>
                                </div>
                            </div>
                        </div>
                        <div class="at-agent-info">
                            <h4><a href="agents-details.html">Martin Guptil</a></h4>
                            <p>sales executive</p>
                        </div>
                    </div>
                    <div class="at-agent-col">
                        <div class="at-agent-img">
                            <img src="https://images.pexels.com/photos/756453/pexels-photo-756453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt="">
                            <div class="at-agent-social">
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <div class="at-agent-call">
                                    <p>Stars</p>
                                </div>
                            </div>
                        </div>
                        <div class="at-agent-info">
                            <h4><a href="agents-details.html">Jesica Mile</a></h4>
                            <p>sales executive</p>
                        </div>
                    </div>
                    <div class="at-agent-col">
                        <div class="at-agent-img">
                            <img src="https://images.pexels.com/photos/756453/pexels-photo-756453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt="">
                            <div class="at-agent-social">
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <div class="at-agent-call">
                                    <p>Stars</p>
                                </div>
                            </div>
                        </div>
                        <div class="at-agent-info">
                            <h4><a href="agents-details.html">Thomas Jons</a></h4>
                            <p>sales executive</p>
                        </div>
                    </div>
                    <div class="at-agent-col">
                        <div class="at-agent-img">
                            <img src="https://images.pexels.com/photos/756453/pexels-photo-756453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt="">
                            <div class="at-agent-social">
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <div class="at-agent-call">
                                    <p>Stars</p>
                                </div>
                            </div>
                        </div>
                        <div class="at-agent-info">
                            <h4><a href="agents-details.html">Cris Jordan</a></h4>
                            <p>sales executive</p>
                        </div>
                    </div>
                    <div class="at-agent-col">
                        <div class="at-agent-img">
                            <img src="https://images.pexels.com/photos/756453/pexels-photo-756453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt="">
                            <div class="at-agent-social">
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <div class="at-agent-call">
                                    <p>Stars</p>
                                </div>
                            </div>
                        </div>
                        <div class="at-agent-info">
                            <h4><a href="agents-details.html">Marri Guptil</a></h4>
                            <p>sales executive</p>
                        </div>
                    </div>
                    <div class="at-agent-col">
                        <div class="at-agent-img">
                            <img src="https://images.pexels.com/photos/756453/pexels-photo-756453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt="">
                            <div class="at-agent-social">
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                <div class="at-agent-call">
                                    <p>Stars</p>
                                </div>
                            </div>
                        </div>
                        <div class="at-agent-info">
                            <h4><a href="agents-details.html">Martin Mozina</a></h4>
                            <p>sales executive</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Agents End -->

{{-- Newsletter starts here --}}
<section class="at-newsletter-sec jarallax at-over-layer-black mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-sm-8">
                <h2>Subscribe <span>Newsletter</span></h2>
                <p>subscribe to our newsletter for some useful content, unsubscribe at anytime</p>
                <form class="input-group">
                    <input type="email" class="form-control" placeholder="Enter Your Email">
                    <div class="input-group-append">
                        <span class="input-group-text at-sub-btn"><a href="#"
                                class="hvr-bounce-to-right">SUBSCRIBE</a></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
{{-- Newsletter Ends here --}}

@endsection