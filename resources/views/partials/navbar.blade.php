<!-- Main Heder Start -->
<section class="at-main-herader-sec">
    <!-- Header top start -->
    <div class="at-header-topbar">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <p><a href="tel:+1 202 110 1122"><i class="icofont icofont-ui-head-phone"></i> +1 202 110 1122</a>
                    </p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <p class="at-respo-right"><a href="mailto:efiwura@gmail.com"><i class="icofont icofont-email"></i>
                            efiwura@gmail.com</a>
                    </p>
                </div>
                {{-- login / register / logout starts here--}}
                <div class="col-lg-4 col-md-3 col-sm-6">
                    {{-- If user is authenticated, show logout, if user is a guest show login and register  --}}
                    @auth

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                    <div class="at-sign-in-up clearfix">
                        <p><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();"><i
                                    class="icofont icofont-sign-in"></i>Logout</a>
                        </p>

                    </div>
                    @endauth
                    @guest
                    <div class="at-sign-in-up clearfix">
                        <p><a href="{{ route('login') }}"><i class="icofont icofont-sign-in"></i> sign in</a>
                        </p>
                        <p> <a href="{{ route('register') }}"><i class="icofont icofont-pencil-alt-2"></i> sign up</a>
                        </p>
                    </div>
                    @endguest

                </div>
                {{-- login / register / logout --}}
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="at-social text-right">
                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header top end -->

    <!-- Header navbar start -->
    <div class="at-navbar fixed-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-6">
                    <div class="main-logo">
                        <a href="{{ route('index') }}"><img src="{{ asset('images/ef.png') }}" alt=""></a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-6 col-6">
                    <div id="main-nav" class="stellarnav">
                        <ul>
                            <li class="current-menu-item menu-item-has-children"><a href="{{ route('index') }}"
                                    @if(Request::route()->getName()==='index') style="color:#D46A21 !important;"
                                    @endif>Home <i class="" aria-hidden="true"></i></a>

                            </li>
                            <li><a href="{{ route('about') }}" @if (Request::route()->getName()==='about')
                                    style="color:#D46A21 !important;" @endif
                                    >About</a></li>
                            <li class="menu-item-has-children"><a href="{{ route('properties') }}"
                                    @if(Request::route()->getName()==='properties' ||
                                    Request::route()->getName()==='details') style="color:#D46A21 !important;"
                                    @endif
                                    >Properties <i class="" aria-hidden="true"></i></a>

                            </li>
                            <li class="menu-item-has-children"><a href="{{ route('faq') }}"
                                    @if(Request::route()->getName()==='faq') style="color:#D46A21 !important;" @endif

                                    >FAQ's <i class="" aria-hidden="true"></i></a>

                            </li>
                            <li class="menu-item-has-children"><a href="{{ route('contact') }}"
                                    @if(Request::route()->getName()==='contact') style="color:#D46A21 !important;"
                                    @endif

                                    >Contact Us <i class="" aria-hidden="true"></i></a>

                            </li>
                            <li class="menu-item-has-children"><a href="{{ route('advertise') }}"
                                    @if(Request::route()->getName()==='advertise') style="color:#D46A21 !important;"
                                    @endif
                                    >Advertise <i class="" aria-hidden="true"></i></a>

                            </li>
                            @auth
                            <li class="menu-item-has-children"><a href="{{ route('home') }}"
                                    @if(Request::route()->getName()==='home') style="color:#D46A21 !important;"
                                    @endif
                                    ><img class="rounded-circle" style="width: 40px !important; height:40px !important;"
                                        src="http://localhost:8000/storage/efiwura/{{ Auth::user()->image }}" alt="">
                                    My Account <i class="" aria-hidden="true"></i></a>

                            </li>
                            @endauth

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header navbar end -->
</section>
<!-- Main Heder End -->