<!-- footer start from here -->
<footer class="at-main-footer at-over-layer-black">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                <div class="at-footer-link-col at-col-default-mar">
                    <h4 style="color:white;">Efiewura</h4>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                </div>
                <p style="color:white">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

            </div>
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                <div class="at-footer-Tag-col at-col-default-mar">
                    <h4 style="color:white;">Address</h4>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>

                </div>
                <div>
                    <p style="color:white">Maxim Nyansa IT Solutions, CORAL AVENUE</p>
                    <p style="color:white">TANTRA HILLS, ACCRA.</p>
                    <p style="color:white"><b>GPS address GW-0845-5256</b></p>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                <div class="at-footer-gallery-col at-col-default-mar">
                    <h4 style="color:white;">Useful links</h4>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 at-social text-left">
                            <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>

                        <div class="col-md-2  at-social text-left">
                            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </div>

                        <div class="col-md-2 at-social text-left">
                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </div>

                        <div class="col-md-2 at-social text-left">
                            <a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer end -->

<!-- Copyright start from here -->
<section class="at-copyright " style="border-top: 1px white solid;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p><a href="{{ route('index') }}" style="color:white;">Efiwura</a> ©2020 || All Rights and Privacy
                    Reserved</p>
            </div>
        </div>
    </div>
</section>