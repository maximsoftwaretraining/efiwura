@extends('layouts.app')

@section('title')
Reset Password
@endsection


@section('content')


<section class="at-agents-sec"
    style="background-image: url('https://images.pexels.com/photos/1571463/pexels-photo-1571463.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940')">
    <div class="container" style=" margin-top:-80px;">

        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="shadow p-4 bg-white" style="border-radius: 20px;">

                    <div class="tab-content" id="myTabContent">
                        <h3 class="text-center">Reset Password</h3>
                        <div role="tabpanel" class="tab-pane fade show active" id="home">
                            @if ($errors->any())
                            <div class="alert alert-danger bg-danger text-white">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach

                                </ul>
                            </div>

                            @endif
                            <form method="POST" action="{{ route('password.update') }}">
                                @csrf

                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="email" name="email"
                                        class="form-control @error('email') is-invalid @enderror" placeholder=""
                                        required>
                                </div>
                                <div class="form-group">
                                    <label for="">New Password</label>
                                    <input type="password" name="password"
                                        class="form-control @error('password') is-invalid @enderror" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="">Confirm Password</label>
                                    <input type="password" name="password_confirmation" class="form-control"
                                        placeholder="">
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-default hvr-bounce-to-right" type="submit">Submit</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->
@endsection