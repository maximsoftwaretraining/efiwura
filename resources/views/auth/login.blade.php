@extends('layouts.app')

@section('title')
Login
@endsection

@section('content')


<section class="at-agents-sec"
    style="background-image: url('https://images.pexels.com/photos/1571463/pexels-photo-1571463.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940')">
    <div class="container" style=" margin-top:-80px;">

        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="shadow p-4 bg-white" style="border-radius: 20px;">
                    <div class="tab-content" id="myTabContent">
                        <h3 class="text-center">Sign In</h3>
                        <div role="tabpanel" class="tab-pane fade show active" id="home">
                            @error('email')
                            <div class="alert alert-danger bg-danger text-white">
                                <strong>{{ $message }}</strong>
                            </div>
                            @enderror
                            <form class="form" action="{{ route('login') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" class="form-control" name="email" placeholder="" required
                                        value="{{ old('email') }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Password</label>
                                    <input type="password" class="form-control" name="password" required placeholder="">

                                </div>
                                <div class="checkbox clearfix">
                                    <label class="pull-left">
                                        <input name="remember" type="checkbox"> Remember Me
                                    </label>
                                    <p class="pull-right">
                                        <a href="{{ route('password.request') }}" class="text-dark">Forgot Your
                                            Password?</a>
                                    </p>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-default btn-block hvr-bounce-to-right" type="submit">
                                        <i class="fa fa-sign-in"></i>
                                        Login
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection