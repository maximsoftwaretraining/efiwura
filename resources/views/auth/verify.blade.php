@extends('layouts.app')


@section('content')
<section class="at-agents-sec" style="background-color: white !important;">
  <div class="container" style=" margin-top:-80px;">
    <div class="row">
      <div class="col-md-12">
        @if (session('resent'))
        <div class="alert alert-success text-center" role="alert">
          {{ __('A fresh verification link has been sent to your email address.') }}
        </div>
        @endif
        <h4 class="text-center">{{ __('Before proceeding, please check your email for a verification link.') }}
          {{ __('If you did not receive the email') }},</h4>
        <h3 class="text-center">
          <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
            @csrf
            <button type="submit" class="btn btn-success text-white">{{ __('click here to request another') }}</button>
        </h3>
        </form>



      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-12">
      </div>
    </div>
  </div>
</section>
@endsection