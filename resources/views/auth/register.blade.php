@extends('layouts.app')

@section('title')
Sign Up
@endsection

@section('content')

<section class="at-agents-sec"
    style="background-image: url('https://images.pexels.com/photos/1571463/pexels-photo-1571463.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940')">
    <div class="container" style=" margin-top:-80px;">

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="shadow p-4 bg-white" style="border-radius: 20px;">


                    <div class="tab-content" id="myTabContent">
                        <h3 class="text-center">Sign Up</h3>
                        <div role="tabpanel" class="tab-pane fade show active" id="profile">
                            @if ($errors->any())
                            <div class="alert alert-danger bg-danger text-white">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach

                                </ul>
                            </div>

                            @endif

                            <form class="form" method="POST" action="{{ route('register') }}"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" name="first_name" value="{{ old('first_name') }}"
                                                class="form-control @error('first_name') is-invalid @enderror" required>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Middle Name</label>
                                            <input type="text" name="middle_name" value="{{ old('middle_name') }}"
                                                class="form-control @error('middle_name') is-invalid @enderror"
                                                placeholder="">

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Sur Name</label>
                                            <input type="text" name="sur_name" value="{{ old('sur_name') }}"
                                                class="form-control @error('sur_name') is-invalid @enderror" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Gender</label>
                                            <select class="form-control @error('gender') is-invalid @enderror"
                                                name="gender" id="">
                                                <option value="">Select One</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="email" name="email" value="{{ old('email') }}"
                                                class="form-control @error('email') is-invalid @enderror" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Contact</label>
                                            <input type="number" name="contact" value="{{ old('contact') }}"
                                                class="form-control @error('contact') is-invalid @enderror" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="image">Profile Image</label>
                                                <input type="file"
                                                    class="form-control @error('image') is-invalid @enderror"
                                                    name="image" id="image">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="image">Address</label>
                                                <input type="text" name="address"
                                                    class="form-control @error('address') is-invalid @enderror"
                                                    value="{{ old('address') }}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="">Password</label>
                                                <input type="password"
                                                    class="form-control @error('password') is-invalid @enderror"
                                                    required name="password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="">Confirm Password</label>
                                                <input type="password"
                                                    class="form-control @error('password_confirmation') is-invalid @enderror"
                                                    name="password_confirmation" placeholder=""> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-block btn-default hvr-bounce-to-right" type="submit">Sign
                                        up</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection