@extends('layouts.app')

@section('title')
Welcome Home
@endsection

@section('content')
{{-- User Dashboard --}}
<section class="at-agents-sec">
    <div class="container" style=" margin-top:-80px;">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="at-sec-title">
                    <h3>Hello <span>{{ Auth::user()->first_name }}</span></h3>
                    <h5>welcome to Efiwura</h5>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-center">
                    This page will be updated soon
                </h4>
            </div>
        </div>
    </div>
</section>
@endsection