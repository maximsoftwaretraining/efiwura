  <!-- Bootstrap -->
    <link  rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">

    <!-- Needed CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/icofont.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/stellarnav.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/featherlight.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/featherlight.gallery.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/hover.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/flexslider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick-theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/animations.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/morphext.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.theme.default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.mb.YTPlayer.min.css')}}">

    <!-- Main stylesheet  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}">
    <!-- Responsive stylesheet  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/efiwura.css')}}">
    <!-- Favicon -->
    <link  rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png')}}">
    <link  rel="icon" type="image/png" href="{{ asset('images/apple-icon.png')}}">
