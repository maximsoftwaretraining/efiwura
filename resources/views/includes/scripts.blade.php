
    <!-- jQuery -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>

    <!-- all plugins and JavaScript -->
    <script type="text/javascript" src="{{asset('js/css3-animate-it.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/stellarnav.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/featherlight.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/featherlight.gallery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.flexslider.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jarallax.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/slick.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery-ui.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery-scrolltofixed-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/morphext.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/dyscrollup.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.ripples.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.mb.YTPlayer.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>

    <!-- Main Custom JS -->
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>