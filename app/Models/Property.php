<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'title',
        'description',
        'owner_id',
        'property_type_id',
        'category_id',
        'price',
        'size',
        'images',
        'features',
        'location_id',
        'address',
        'latitude',
        'longitude',
        'status',
        'available',
        'slug',
    ];
    
    protected $casts=[
        'images'=>'array',
    ];

    //gets all the comments on this property
    public function comments() {
    	return $this->hasMany(Comment::class);
    }

    //gets the owner of this property
    public function owner()
    {
        return $this->belongsTo(User::class,'owner_id');
    }

    //gets the location of this property
    public function location()
    {
    return $this->belongsTo(Location::class);
    }

    //gets the category of this property
    public function category()
    {
    return $this->belongsTo(Category::class);
    }
    //gets the property_type of this property
    public function propertyType()
    {
    return $this->belongsTo(PropertyType::class);
    }

    public function views()
    {
    return $this->hasMany(View::class);
    }

    //location_name accessor
    public function getLocationNameAttribute(){
    return $this->location()->first()->name;
    }

    //convert images to array
    public function getPropertyImagesAttribute()
    {
    return json_decode($this->images);
    }
}
