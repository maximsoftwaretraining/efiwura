<?php

namespace App\Models;

use App\Jobs\SendVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'sur_name',
        'email',
        'image',
        'password',
        'gender',
        'address',
        'contact',
        'role_id',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //gets the role of this user
    public function role()
    {
        return $this->belongsTo('App\Models\Role','id');
    }

    //gets all properties by this user
    public function properties()
    {
        return $this->hasMany('App\Models\Property','owner_id','id');
    }

    //gets all ratings for this user
    public function ratings()
    {
       return $this->hasMany('App\Models\Rating','rated_user');
    }

    //gets all views on users properties
    public function views()
    {
       return $this->hasManyThrough('App\Models\View','App\Models\Property','owner_id');
    }

    //gets the avg of all ratings of a user
    public function getUserRatingAttribute()
    {
    return collect($this->ratings()->get('rate')->toArray())->avg('rate');
    }

    //Override sendEmailVerificationNotification method from MustVerifyEmail 
    //and dispatch to a job to decrease response time
    
    public function sendEmailVerificationNotification()
    {
    SendVerifyEmail::dispatch($this);
    }

}
