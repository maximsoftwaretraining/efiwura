<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Listing extends Model
{
    use softDeletes;

    protected $fillable =[
            'property_id', 'from', 'to','active'
    ];

    //gets the property for this listing
    public function property()
    {
    return $this->belongsTo(Property::class);
    }
}
