<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyType extends Model
{

    use SoftDeletes;

    protected  $fillable = [
       'name'
    ];

    //gets all the property under this Type
    public function properties()
    {
        return $this->hasMany('App\Models\Property');
    }

}
