<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //gets all the properties in this location
    public function properties()
    {
        return $this->hasMany('App\Models\Property');
    }
}
