<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'property_id','body'
    ];

    //gets the user who posted this comment
    public function commentBy()
    {
    return $this->belongsTo('App\Models\User','commenter_id');
    }
}
