<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
       
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:50'],
            'middle_name' => ['nullable', 'string', 'max:50'],
            'sur_name' => ['required', 'string', 'max:50'],
            'gender' => ['required', 'string', 'max:50'],
            'image' => ['nullable', 'file','mimes:png,jpg,jpeg'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'contact' => ['required', 'string','unique:users','max:15'],
            'address'=>['required','string','max:100'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
      
        
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        //profile pic is optional so check if user uploaded an image
        if(request()->hasFile('image')){

            $image_file = $data['image'];

            $extension =$image_file->getClientOriginalExtension();

            $new_name = $data['first_name'].'_'.Str::uuid().'.'.$extension;
          
            $path = $image_file->storeAs('profile_pictures',$new_name);
            
            $data['image'] = $path;
        }else{
            //use a default image if user did not upload an image,
            //this will be changed to a gravatar later on
            $data['image'] = 'profile_pictures/default.png';
        }
        
        $data['password'] = Hash::make($data['password']);

        //The default role of a user is member,
        //member is the first role with id=1 
        $data['role_id'] = 1;
        $user = User::create($data);
        return $user;
    }
}
