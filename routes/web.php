<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes(['verify' => true]);

Route::get('/','Efiwura\PagesController@index')->name('index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/properties','Efiwura\PagesController@showPropertiesPage')->name('properties');

Route::get('/about',  'Efiwura\PagesController@about')->name('about');

Route::get('/faq',  'Efiwura\PagesController@faq')->name('faq');

Route::get('contact', 'Efiwura\PagesController@showContactPage')->name('contact');

Route::get('/properties/{slug}','Efiwura\PagesController@showPropertyDetailsPage')->name('details');

Route::get('/advertise','Efiwura\PagesController@showAdvertisePage')->name('advertise')->middleware('verified');

Route::resource('property', 'Efiwura\PropertiesController')->middleware('verified');