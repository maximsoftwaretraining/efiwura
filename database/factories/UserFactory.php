<?php

use App\Models\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'middle_name' => $faker->name,
        'sur_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'image' => $faker->randomElement(['profile_pictures/1.jpg','profile_pictures/2.jpg','profile_pictures/3.jpg','profile_pictures/4.jpg','profile_pictures/5.jpg']),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'gender' => 'Male',
        'address' => $faker->word,
        'contact' => $faker->phoneNumber,
        'role_id' => 1,
        'email_verified_at'=>now(),
        'remember_token' => Str::random(10),
    ];
});

