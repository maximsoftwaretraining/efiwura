<?php

use App\Models\Comment;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'commenter_id' => $faker->numberBetween(1,5),
        'property_id' => $faker->numberBetween(1,100),
        'body' => $faker->sentence,

    ];
});
