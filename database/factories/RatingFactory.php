<?php

use App\Models\Rating;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Rating::class, function (Faker $faker) {
    return [
        'rater_id' => $faker->randomDigit,
        'rated_user' => $faker->randomDigit,
        'rate' => $faker->randomFloat(2,1,5),

    ];
});
