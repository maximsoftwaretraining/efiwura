<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\View;
use Faker\Generator as Faker;

$factory->define(View::class, function (Faker $faker) {
    return [
    'property_id'=>$faker->numberBetween(1,100),
    'count'=>1
    ];
});
