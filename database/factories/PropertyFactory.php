<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Property;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Property::class, function (Faker $faker) {
    return [
        //
        'title'=>$title=$faker->word,
        'description'=>$faker->sentence,
        'owner_id'=>$faker->numberBetween(1,3),
        'property_type_id'=>$faker->numberBetween(1,10),
        'category_id'=>$faker->numberBetween(1,2),
        'price'=>$faker->randomFloat(2,100,1000000),
        'images'=>json_encode($faker->randomElements(['property_images/1.jpeg','property_images/2.jpeg','property_images/3.jpeg','property_images/4.jpeg','property_images/5.jpeg','property_images/6.jpeg','property_images/7.jpeg','property_images/8.jpeg','property_images/9.jpeg','property_images/10.jpeg'],6,false)),
        'features'=>"Area 40 Acres",
        'location_id'=>$faker->numberBetween(1,10),
        'address'=>$faker->address,
        'latitude'=>$faker->randomFloat(5,0,1000000),
        'longitude'=>$faker->randomFloat(5,0,1000000),
        'status'=>$faker->randomElement(['New Building','Old Building','Uncompleted']),
        'slug'=>Str::slug($title),
    ];
});
