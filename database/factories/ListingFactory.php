<?php

use App\Models\Listing;
use Illuminate\Support\Str;
use Faker\Generator as Faker;


$factory->define(Listing::class, function (Faker $faker) {
    return [
        'property_id' => $faker->numberBetween(1,100),
        'from' => now(),
        'to' => now()->addDays(10),
    ];
});
