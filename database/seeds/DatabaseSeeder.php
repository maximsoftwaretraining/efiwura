<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(CommentTableSeeder::class);
        $this->call(ListingsTableSeeder::class);
        $this->call(PropertyTypeTableSeeder::class);
        $this->call(RatingTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PropertyTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(ViewTableSeeder::class);
    }
}
