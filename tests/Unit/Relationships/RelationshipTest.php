<?php

namespace Tests\Unit\Relationships;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Listing;
use App\Models\Location;
use App\Models\Property;
use App\Models\PropertyType;
use App\Models\Rating;
use App\Models\Role;
use App\Models\User;
use App\Models\View;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;

class RelationshipTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUserBelongsToARole()
    {
        $role = factory(Role::class, 1)->create();
        $user = factory(User::class, 1)->create(['role_id'=>$role->first()->id]);
        $user_role = $user->first()->role()->get();

        $this->assertInstanceOf(Role::class,$user_role->first());
        $this->assertSame($user_role->count(),1);
    }
    public function testRoleHasManyUsers()
    {
        $role = factory(Role::class, 1)->create();
        factory(User::class, 5)->create(['role_id'=>$role->first()->id]);
        $role_users = $role->first()->users()->get();

        $this->assertInstanceOf(Collection::class,$role_users);
        $this->assertSame($role_users->count(),5);
    }
    public function testUserHasManyProperties()
    {
        $user = factory(User::class, 1)->create(); // a user registers
        factory(Property::class, 5)->create(['owner_id'=>$user->first()->id]); // the user registers properties
        $user_properties = $user->first()->properties()->get(); // so the user should have many properties

        $this->assertInstanceOf(Collection::class,$user_properties); //user_properties must be a collection
        $this->assertSame($user_properties->count(),5); // the user registered 5 properties so, the count
        //of user's properties should be 5.
    }
    public function testPropertyBelongsToAnOwner()
    {
        $user = factory(User::class, 1)->create();
        $property = factory(Property::class, 1)->create(['owner_id'=>$user->first()->id]);
        $property_user = $property->first()->owner()->get();

        $this->assertInstanceOf(User::class,$property_user->first());
        $this->assertSame($property_user->first()->id,$user->first()->id);
    }
    public function testUserHasManyRatings()
    {
        $user = factory(User::class, 1)->create();
        factory(Rating::class, 5)->create(['rated_user'=>$user->first()->id]);
        $user_ratings = $user->first()->ratings()->get();

        $this->assertInstanceOf(Collection::class,$user_ratings);
        $this->assertSame($user_ratings->count(),5);
    }

    public function testUserHasManyViews()
    {
    $user = factory(User::class,1)->create(); //a user registers
    $property = factory(Property::class,1)->create(['owner_id'=>$user->first()->id]); // a user registers a propert
    factory(View::class,5)->create(['property_id'=>$property->first()->id]); // the property is viewed by 5 users
    $user_views = $user->first()->views()->get(); //get the views by on the users property
    $this->assertInstanceOf(Collection::class,$user_views); // the users views must be a collection
    $this->assertSame($user_views->count(),5); // 5 people viewed so count must be 5
    }

    public function testPropertyHasManyComments()
    {
        $property = factory(Property::class, 1)->create();
        factory(Comment::class, 5)->create(['property_id'=>$property->first()->id]);
        $property_comments = $property->first()->comments()->get();

        $this->assertInstanceOf(Collection::class,$property_comments);
        $this->assertSame($property_comments->count(),5);
    }

    public function testPropertyBelongsToALocation()
    {
        $location = factory(Location::class, 1)->create();
        $property = factory(Property::class, 1)->create(['location_id'=>$location->first()->id]);
        $property_location = $property->first()->location()->get();

        $this->assertInstanceOf(Collection::class,$property_location);
        $this->assertSame($property_location->first()->id,$location->first()->id);
    }
    public function testPropertyBelongsToACategory()
    {
        $category = factory(Category::class, 1)->create();
        $property = factory(Property::class, 1)->create(['category_id'=>$category->first()->id]);
        $property_category = $property->first()->category()->get();

        $this->assertInstanceOf(Category::class,$property_category->first());
        $this->assertSame($property_category->first()->id,$category->first()->id);
    }

    public function testPropertyBelongsToAPropertyType()
    {
        $property_type = factory(PropertyType::class, 1)->create();
        $property = factory(Property::class, 1)->create(['property_type_id'=>$property_type->first()->id]);
        $property_p_type = $property->first()->propertyType()->get();

        $this->assertInstanceOf(PropertyType::class,$property_p_type->first());
        $this->assertSame($property_p_type->first()->id,$property_type->first()->id);
    }

    public function testLocationHasManyProperties()
    {
        $location = factory(Location::class, 1)->create();
        factory(Property::class, 5)->create(['location_id'=>$location->first()->id]);
        $location_properties= $location->first()->properties()->get();

        $this->assertInstanceOf(Collection::class,$location_properties);
        $this->assertSame($location_properties->count(),5);
    }
    public function testPropertyTypeHasManyProperties()
    {
        $property_type = factory(PropertyType::class, 1)->create();
        factory(Property::class, 5)->create(['property_type_id'=>$property_type->first()->id]);
        $p_type_properties= $property_type->first()->properties()->get();

        $this->assertInstanceOf(Collection::class,$p_type_properties);
        $this->assertSame($p_type_properties->count(),5);
    }

    public function testListingBelongsToAProperty()
    {
        $property = factory(Property::class, 1)->create();
        $listing = factory(Listing::class, 1)->create(['property_id'=>$property->first()->id]);
        $listing_property = $listing->first()->property()->get();

        $this->assertInstanceOf(Property::class,$listing_property->first());
        $this->assertSame($listing_property->first()->id,$property->first()->id);
    }
    public function testCommentBelongsToAUser()
    {
        $user = factory(User::class, 1)->create();
        $comment = factory(Comment::class, 1)->create(['commenter_id'=>$user->first()->id]);
        $comment_author = $comment->first()->commentBy()->get();

        $this->assertInstanceOf(User::class,$comment_author->first());
        $this->assertSame($comment_author->first()->id,$user->first()->id);
    }
    public function testCategoryHasManyProperties()
    {
            $category = factory(Category::class, 1)->create();
            factory(Property::class, 5)->create(['category_id'=>$category->first()->id]);
            $category_properties= $category->first()->properties()->get();

            $this->assertInstanceOf(Collection::class,$category_properties);
            $this->assertSame($category_properties->count(),5);
        }



}
