<?php

namespace Tests\Feature\Account;

use App\Models\Role;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UserAccountTest extends TestCase
{
    use WithFaker,RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUserCanCreateAccountWithProfilePicture()
    {
        $this->withoutExceptionHandling();

        Session::start();
        Storage::fake('images');
        $image_file = UploadedFile::fake()->image('image.jpg');

        factory(Role::class,1)->create();
        $response = $this->post('/register',[
            'first_name' => $this->faker->name,
            'middle_name' => $this->faker->name,
            'sur_name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'image' => $image_file,
            'password' => 'password',
            'password_confirmation'=>'password',
            'gender' => 'Male',
            'address' => $this->faker->word,
            'contact' => '02335677289',
            '_token' =>csrf_token(),
        ]);
        
        $response->assertStatus(302);
        $response->assertRedirect('/home');
    }
    public function testUserCanCreateAccountWithoutProfilePicture()
    {
        $this->withoutExceptionHandling();

        Session::start();
    
        factory(Role::class,1)->create();
        $response = $this->post('/register',[
            'first_name' => $this->faker->name,
            'middle_name' => $this->faker->name,
            'sur_name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => 'password',
            'password_confirmation'=>'password',
            'gender' => 'Male',
            'address' => $this->faker->word,
            'contact' => '02335677289',
            '_token' =>csrf_token(),
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
    }
    
}
